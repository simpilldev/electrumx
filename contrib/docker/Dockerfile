# slightly altered from upstream ../Dockerfile
FROM python:3.9.4-buster AS builder

WORKDIR /usr/src/app

RUN apt-get update \
    && apt-get -y --no-install-recommends install \
        librocksdb-dev libsnappy-dev libbz2-dev libz-dev liblz4-dev libcurl4-openssl-dev \
    && rm -rf /var/lib/apt/lists/*

COPY . .
RUN python -m venv venv \
    && venv/bin/pip3 install --no-cache-dir -e .[rapidjson,rocksdb,uvloop]

FROM python:3.9.4-slim-buster

RUN apt-get update \
    && apt-get -y --no-install-recommends install \
        librocksdb5.17 libsnappy1v5 libbz2-1.0 zlib1g liblz4-1 openssl \
    && rm -rf /var/lib/apt/lists/*

ENV SERVICES="tcp://:50001,ws://:50004"
ENV COIN=PandacoinRegtest
ENV DAEMON_URL="@localhost:44444"
ENV DB_DIRECTORY=/var/lib/electrumx
ENV ALLOW_ROOT=true
ENV EVENT_LOOP_POLICY=uvloop
ENV DB_ENGINE=rocksdb
ENV MAX_SEND=10000000
ENV BANDWIDTH_UNIT_COST=50000
ENV CACHE_MB=2000

WORKDIR /usr/src/app
COPY --from=builder /usr/src/app .
COPY contrib/docker/init /usr/local/bin/init

VOLUME /var/lib/electrumx

RUN mkdir -p "$DB_DIRECTORY" && ulimit -n 1048576

CMD ["/usr/src/app/venv/bin/python", "/usr/src/app/venv/bin/electrumx_server"]
